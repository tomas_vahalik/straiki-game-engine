using System.Collections.Generic;
using Microsoft.Xna.Framework;
using StraikiEngine;

namespace MulGame.Components
{
    public class Map : Component
    {
        private readonly List<Component> _components;

        public Map() : base("map", true)
        {
            _components = new List<Component>();
        }

        // 0 nothing
        // 1 floor
        // 99 start
        // 100 finish
        public Vector2 LoadMap(int id)
        {
            ClearMap();
            var maps = new List<string[]>();
            string [] lines =
                {
                    "0,0,0,0,100",
                    "0,0,1,1,1",
                    "0,0,1,0,0",
                    "0,1,1,0,0",
                    "99,1,0,0,0"
                };
            maps.Add(lines);
            string[] lines2 =
                {
                    "100,1,1,1,1",
                      "0,0,0,0,1",
                      "0,0,1,1,1",
                      "0,1,1,0,0",
                     "99,1,0,0,0"
                };
            maps.Add(lines2);

            lines = maps[id];

            var retVal = Vector2.Zero;
            for (var i = 0; i < lines.Length; i++)
            {
                var t = lines[i];
                var line = t.Split(',');
                // naparsov�n� hodnot v ��dku
                for (var j = 0; j < line.Length; j++)
                {
                    var t1 = line[j];
                    var type = -1;
                    int.TryParse(t1, out type);

                    Component c = null;
                    switch (type)
                    {
                        case 0:
                            break;
                        case 1:
                            c = new Floor(j,i);
                            break;
                        case 99:
                            c = new StartFloor(j,i);
                            retVal = new Vector2(j,i);
                            var tmpC = new PlayCube(j, i);
                            _components.Add(tmpC);
                            break;
                        case 100:
                            c = new StartFloor(j, i, true);
                            break;
                    }

                    if (c == null) continue;

                    Parent.AddComponent(c);
                    _components.Add(c);
                }

            }

            return retVal;
        }

        //private static bool loaded = false;

        //public override void Update()
        //{
        //    if (loaded) return;

        //    loaded = true;

        //    foreach (var component in _components)
        //    {
        //        Parent.AddComponent(component);
        //    }
        //}


        private void ClearMap()
        {
            foreach (var component in _components)
            {
                Parent.RemoveComponent(component);
            }
            _components.Clear();
        }
    }
}
