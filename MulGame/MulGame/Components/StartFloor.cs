using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using StraikiEngine;
using StraikiEngine.Components;

namespace MulGame.Components
{
    public class StartFloor : Model3D
    {
        public bool IsEnd = false;

        public StartFloor(int x, int z, bool end) : this(x, z)
        {
            IsEnd = end;
        }

        public StartFloor(int x, int z) : base("start", new Vector3(x*100,0,z*100))
        {
        }
    }
}
