using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using MulGame.GameScreens;
using StraikiEngine;
using StraikiEngine.Cameras;
using StraikiEngine.Components;

namespace MulGame.Components
{
    public class PlayCube : Model3D
    {
        private Vector2 _dir;
        private Vector3 _startPosition;

        public PlayCube(int x, int z)
            : base("playCube", new Vector3(x * 100, 100, z * 100))
        {
            _startPosition = new Vector3(x*100, 100, z*100);
        }

        private bool _falling = false;
        private bool _winning = false;

        private Vector3 _endPosition = new Vector3(-9999,-9999,-9999);
        private List<Vector3> _positions = new List<Vector3>(); 

        public void MoveCube(int direction)
        {
            //_length = TimeSpan.FromSeconds(0.5);
            //_transitionValue = 0f;
            const int count = 10;
            _step = MoveDelta(100, count, direction);
            _isMoving = true;
            _newPos = Position + _step*count;

            if (_positions.Count == 0)
            {
                var possibleCubes = from c in Parent.Components.OfType<Floor>() select c;
                foreach (var possibleCube in possibleCubes)
                {
                    _positions.Add(possibleCube.Position);
                }
            }
            _falling = true; // predpoklad ze pada
            foreach (var position in _positions)
            {
                if (Math.Abs(_newPos.X - position.X) < 0.001 && Math.Abs(_newPos.Z - position.Z) < 0.001)
                {
                    _falling = false;
                    break;
                }
            }
               
            if (_falling && (Math.Abs(_newPos.X - _startPosition.X) < 0.001 && Math.Abs(_newPos.Z - _startPosition.Z) < 0.001))
                _falling = false;
            else if (_falling && (Math.Abs(_newPos.X - _endPosition.X) < 0.001 && Math.Abs(_newPos.Z - _endPosition.Z) < 0.001))
                _falling = false;


            if (_endPosition.Equals(new Vector3(-9999,-9999,-9999)))
            {
                var endPosition = from c in Parent.Components.OfType<StartFloor>() where c.IsEnd select c.Position;
                foreach (var pos in endPosition)
                {
                    if (Math.Abs(pos.X - _startPosition.X) > 0.001 || Math.Abs(pos.Z - _startPosition.Z) > 0.001)
                        _endPosition = pos;
                }
            }

            if (Math.Abs(_newPos.X - _endPosition.X) < 0.001 && Math.Abs(_newPos.Z - _endPosition.Z) < 0.001)
            {
                _winning = true;
            }

            Parent.Engine.SoundManager.PlaySound(0);
        }

        //private TimeSpan _length;
        private bool _isMoving = false;
        private Vector3 _step;
        private Vector3 _newPos;
        //private TimeSpan _timeSpan;
        //private float _transitionValue;

        public override void Update()
        {
            
            
            if (!_isMoving)
            {
                if(Parent.Camera.Active == true) return;
                
                var direction = GetDirection();
                if (direction == -1) return;
                MoveCube(direction);    
            }
            else
            {
                Position += _step;
                if (Position.Equals(_newPos))
                {
                    _isMoving = false;
                    if (_falling)
                    {
                        Position = _startPosition;
                        _falling = false;
                    }
                    else if (_winning)
                    {

                        Parent.Finished = true;
                        //Parent.Engine.PopGameScreen();
                        Parent.NextLevel();
                    }
                }    
            }
            
            
        }
        
        private int GetDirection()
        {
            if (!Parent.Engine.Input.Flick) return -1;
            
            _dir = Parent.Engine.Input.FlickDelta;

            if (Math.Abs(_dir.X) > Math.Abs(_dir.Y) && _dir.X > 0)
                return 1; // right
            
            if (Math.Abs(_dir.X) > Math.Abs(_dir.Y) && _dir.X < 0)
                return 3; // left
            
            if (Math.Abs(_dir.X) < Math.Abs(_dir.Y) && _dir.Y > 0)
                return 0; // up
            
            if (Math.Abs(_dir.X) < Math.Abs(_dir.Y) && _dir.Y < 0)
                return 2; // down

            return -1;
        }

        private static Vector3 MoveDelta(int size, int steps, int direction)
        {
            var num = (float) size/steps;
            var res = Vector3.Zero;
            switch (direction)
            {
                case 0://up
                    res = new Vector3(0,0,num);
                    break;
                case 1://right
                    res = new Vector3(num, 0, 0);
                    break;
                case 2://down
                    res = new Vector3(0, 0, -num);
                    break;
                case 3://left
                    res = new Vector3(-num, 0, 0);
                    break;
                default:
                    break;
                    
            }
            return res;
        }

        //private void UpdateTransition(GameTime elapsedTime)
        //{
        //    var transitionDelta = (float) (elapsedTime.ElapsedGameTime.TotalMilliseconds/_length.TotalMilliseconds);
        //    _transitionValue += transitionDelta;

        //    if (!(_transitionValue >= 1)) return;

        //    _transitionValue = 1f;
        //    _isMoving = false;
        //}
    }
}
