using System.Collections.Generic;
using System.Globalization;
using Microsoft.Xna.Framework;
using MulGame.Components;
using System.Linq;
using StraikiEngine;
using StraikiEngine.Cameras;
using StraikiEngine.Components;

namespace MulGame.GameScreens
{
    public class Level1 : GameScreen
    {
        public Level1() : base("Lvl1")
        {
        }

        private readonly List<Component> _labels = new List<Component>();
        private MiniGui _gui;

        private Map _lvl1 = new Map();
        private PlayCube _cube;

        protected override void Load()
        {
            //base.Load();
            AddComponent(new Background(Color.Orange));
            //AddComponent(new Sprite("straiki", new Vector2(100, 120)));
            _labels.Add(new TextLabel(Engine.Input.MoveDelta.ToString(), new Vector2(15, 75)));

            this.Camera = new ArcBallCamera(this, new Vector3(250, 600, 800), 670f); //cum na stred
            AddComponent(new GuideLines(100, 11, true));
            
            
            AddComponent(_lvl1);
            var cubePos = _lvl1.LoadMap(0);
            _cube = new PlayCube((int)cubePos.X, (int)cubePos.Y);
            AddComponent(_cube);
            _gui = new MiniGui(true);
            AddComponent(_gui);
            _gui.LoadBtns();

            Engine.SoundManager.Music(true);
        }

        public override void NextLevel()
        {
            RemoveComponent(_lvl1);
            _lvl1 = new Map();
            AddComponent(_lvl1);

            RemoveComponent(_cube);
            var cubePos = _lvl1.LoadMap(1);
            _cube = new PlayCube((int)cubePos.X, (int)cubePos.Y);
            AddComponent(_cube);
        }



        private string _txtInfo = "Pohyb kamery";

        public override void Update()
        {
            base.Update();
            foreach (var component in _labels)
            {
                RemoveComponent(component);    
            }
            _labels.Clear();
            
            var pressed = Engine.Input.TouchPoint;
            
            var btns = from c in Components.OfType<Button>() select c;
            foreach (Button button in btns.Where(button => button.WasPressed(pressed)))
            {
                switch (button.GetId())
                {
                    case (int) StraikiEngine.Components.MiniGui.Buttons.PLAY:
                        this.Camera.Active = false;
                        _txtInfo = "Pohyb kostkou!";
                        
                        break;
                    case (int) StraikiEngine.Components.MiniGui.Buttons.ROTATE:
                        this.Camera.Active = true;
                        _txtInfo = "Pohyb kamery!";
                        break;
                }
                
            }

            var label = new TextLabel(_txtInfo, new Vector2(70, 10));
            _labels.Add(label);
            label = new TextLabel("Position: " + Camera.LookAt.X.ToString(CultureInfo.InvariantCulture) + " : " + Camera.LookAt.Y.ToString(CultureInfo.InvariantCulture) + " : " + Camera.LookAt.Z.ToString(CultureInfo.InvariantCulture), new Vector2(70, 40));
            _labels.Add(label);
            label = new TextLabel("Zoom: " + Camera.Zoom, new Vector2(70, 70));
            _labels.Add(label);
            foreach (var component in _labels)
            {
                AddComponent(component);
            }
        }
    }
}
