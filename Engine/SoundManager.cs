using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Media;

namespace StraikiEngine
{
    public class SoundManager
    {
        private readonly ContentManager _cm;

        public SoundManager(ContentManager cm)
        {
            _cm = cm;
            Load();
        }

        private SoundEffect _soundMove;
        private SoundEffect _soundWin;
        private Song _music;

        protected void Load()
        {
            _soundMove = _cm.Load<SoundEffect>("Audio\\Sounds\\move");
            _soundWin = _cm.Load<SoundEffect>("Audio\\Sounds\\applause");
            _music = _cm.Load<Song>("Audio\\Music\\background");
        }

        public void PlaySound(int index)
        {
            switch (index)
            {
                case 0:
                    _soundMove.Play();
                    break;
                case 1:
                    _soundWin.Play();
                    break;
            }
        }

        public void Music(bool play)
        {
            if (play)
            {
                MediaPlayer.Play(_music);
            }
        }

        public void Update()
        {
            if(MediaPlayer.State == MediaState.Stopped || MediaPlayer.State == MediaState.Paused)
                Music(true);
        }
    }
}
