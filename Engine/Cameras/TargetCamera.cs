using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace StraikiEngine.Cameras
{
    public class TargetCamera : Camera
    {
        public Vector3 Target { set; get; }

        public TargetCamera(GameScreen parent, Vector3 position)
            : this(parent, position, Vector3.Zero)
        {
        }

        public TargetCamera(GameScreen parent, Vector3 position, Vector3 target)
            : base(parent)
        {
            Position = position;
            Target = target;
        }

        public override void Update()
        {
            this.View = Matrix.CreateLookAt(Position, Target, Vector3.Up);
        }
    }
}
