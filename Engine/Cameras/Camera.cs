
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace StraikiEngine.Cameras
{
    public abstract class Camera
    {
        public Matrix Projection { get; set; }
        public Matrix View { get; set; }
        public Vector3 Position { get; set; }
        //public float Zoom { get; set; }

        public bool Active = true;

        private float _farPlane;
        public float FarPlane
        {
            get { return _farPlane; }
            set
            {
                _farPlane = value;
                Projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4,
                                                                 Parent.Engine.GraphicsDevice.Viewport.AspectRatio, 0.1f, _farPlane);   
            }
        }

        protected GameScreen Parent;

        public Camera(GameScreen parent)
        {
            Parent = parent;
            FarPlane = 10000f;

            Projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4,
                                                             Parent.Engine.GraphicsDevice.Viewport.AspectRatio, 0.1f,
                                                             _farPlane);
        }

        public virtual void Update()
        {
            
        }
    }
}
