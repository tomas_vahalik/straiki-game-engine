using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace StraikiEngine.Components
{
    public class MiniGui : Component
    {
        private readonly List<Component> _components;

        public int PressedButton = 0;

        public enum Buttons
        {
            //MOVE = 0,
            NONE = 0,
            ROTATE,
            PLAY,
            MOVE
        };

        public MiniGui(bool visible) : base("GUI", visible)
        {
            _components = new List<Component>();

            const int size = 80;

            var btn1 = new Button("Buttons\\play", (int) Buttons.PLAY, new Vector2(10, 10));
            var btn2 = new Button("Buttons\\rotate", (int) Buttons.ROTATE, new Vector2(10, 10 + size));
            _components.Add(btn1);
            _components.Add(btn2);

        }

        public void LoadBtns()
        {
            foreach (var component in _components)
            {
                Parent.AddComponent(component);
            }
        }

        private static bool _loaded = false;

        public void Clear()
        {
            foreach (var component in _components)
            {
                Parent.RemoveComponent(component);
            }
            _components.Clear();
        }
    }
}
