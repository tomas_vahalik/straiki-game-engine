using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace StraikiEngine.Components
{
    public class Model3D : Component
    {
        public Vector3 Position { get; set; }
        public Matrix Rotation { get; set; }
        public Vector3 Scale { get; set; }
        public string ModelName { get; set; }

        protected Model Model;
        private Matrix[] _transformations;

        public Model3D(string name, Vector3 position) : this(name, position, Matrix.Identity)
        {   
        }

        public Model3D(string name, Vector3 position, Matrix rotation)
            : this(name, position, rotation, Vector3.One)
        {
        }

        public Model3D(string name, Vector3 position, Matrix rotation, Vector3 scale) : base("Model3D_" + name)
        {
            Position = position;
            Rotation = rotation;
            Scale = scale;
            ModelName = name;
        }
        /// <summary>
        /// Load desired model with transformations
        /// </summary>
        public override void Load()
        {
            Model = Parent.Engine.ContentManager.Load<Model>("Models/" + ModelName);
            _transformations = new Matrix[Model.Bones.Count];
            Model.CopyBoneTransformsTo(_transformations);
        }
        /// <summary>
        /// Draw model-mesh
        /// </summary>
        /// <param name="view"></param>
        /// <param name="projection"></param>
        /// <param name="camPosition"></param>
        public override void Draw(Matrix view, Matrix projection, Vector3 camPosition)
        {
            var world = Utils.Utils.CreateWorld(Position);

            foreach (var mesh in Model.Meshes)
            {
                foreach (var efect in from part in mesh.MeshParts where part.Effect is BasicEffect select part.Effect as BasicEffect)
                {
                    efect.EnableDefaultLighting();
                    efect.View = view;
                    efect.Projection = projection;
                    efect.World = _transformations[mesh.ParentBone.Index] * world;
                }
                mesh.Draw();
            }
        }
        
    }
}
