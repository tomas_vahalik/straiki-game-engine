using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace StraikiEngine.Components
{
    public class GuideLines : Component
    {
        private VertexPositionColor[] _points;
        private BasicEffect _effect;
        private readonly int _step;
        private readonly int _num;

        private static readonly Dictionary<String,VertexPositionColor[]> PointsCache = new Dictionary<String,VertexPositionColor[]>();

        public GuideLines(int step, int num , bool visible) : base("Mrizka", visible)
        {
            this._step = step;
            this._num = num;
            this._points = new VertexPositionColor[_num* 4];
            
            if (PointsCache.ContainsKey(step + "_" + num))
            {
                this._points =  PointsCache[step + "_" + num];
            }
            else
            {
                var k = 0;
                var color = Color.Gray;
                for (var i = 0; i < _num; i++)
                {
                    var tmp = Vector3.Left * _step * i;
                    if (_num / 2 == i)
                    {
                        _points[k] = new VertexPositionColor(tmp, Color.Red);
                        _points[k + 1] = new VertexPositionColor(tmp + Vector3.Forward * (_step * (_num - 1)), Color.Red);
                        tmp = Vector3.Forward * _step * i;
                        _points[k + 2] = new VertexPositionColor(tmp, Color.Green);
                        _points[k + 3] = new VertexPositionColor(tmp + Vector3.Left * (_step * (_num - 1)), Color.Green);
                    }
                    else
                    {
                        _points[k] = new VertexPositionColor(tmp, color);
                        _points[k + 1] = new VertexPositionColor(tmp + Vector3.Forward * (_step * (_num - 1)), color);
                        tmp = Vector3.Forward * _step * i;
                        _points[k + 2] = new VertexPositionColor(tmp, color);
                        _points[k + 3] = new VertexPositionColor(tmp + Vector3.Left * (_step * (_num - 1)), color);
                    }

                    k += 4;
                }
                PointsCache.Add(step + "_" + num, this._points);
            }
        }

        public override void Draw(Matrix view, Matrix projection, Vector3 camPosition)
        {
            //base.Draw(view, projection, camPosition);
            this._effect = new BasicEffect(Parent.Engine.GraphicsDevice) {VertexColorEnabled = true};
            _effect.View = view;
            _effect.Projection = projection;
            _effect.World = Matrix.CreateTranslation(new Vector3(_num * (_step / 2), 0, _num * (_step / 2)));
            _effect.CurrentTechnique.Passes[0].Apply();
            Parent.Engine.GraphicsDevice.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.LineList, _points, 0, _points.Length / 2);
        }

    }
}
