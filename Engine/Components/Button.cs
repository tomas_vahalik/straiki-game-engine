using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace StraikiEngine.Components
{
    public class Button : Sprite
    {
        public Rectangle ButtonRect;
        private static int _size = 60;
        private int _id = 0;

        public Button(string texName, int id, Vector2 pos)
            : this(texName, pos, _size, _size)
        {
            ButtonRect = new Rectangle((int)pos.X, (int)pos.Y, _size, _size);
            _id = id;
        }

        public Button(string texName, Vector2 pos, int width, int height) : base(texName, pos, width, height)
        {
        }

        public int GetId()
        {
            return _id;
        }

        public bool WasPressed(Point position)
        {
            return ButtonRect.Contains((int)position.X, (int)position.Y);
        }

    }
}
