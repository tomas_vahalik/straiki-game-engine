using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace StraikiEngine.Components
{
    public class TextLabel : Component
    {
        private const string FontName = "Verdana";
        public Vector2 Position { get; set; }
        public Color Color { get; set; }
        public string Text { get; set; }

        protected SpriteFont SpriteFont;

       public TextLabel(string text, Vector2 position, string componentName) : this(text, Color.White, position, componentName){}
        public TextLabel(string text, Vector2 position) : this(text, Color.White, position, "TextLabel"){}
        
        public TextLabel(string text, Color color, Vector2 position, string componentName) : base(componentName, true)
        {
            Position = position;
            Color = color;
            Text = text;
        }

        public override void Load()
        {
            SpriteFont = Parent.Engine.ContentManager.Load<SpriteFont>(FontName);
        }

        public override void Draw()
        {
            Parent.Engine.SpriteBatch.Begin();
            Parent.Engine.SpriteBatch.DrawString(SpriteFont,Text,Position,Color);
            Parent.Engine.SpriteBatch.End();
        }
    }
}
