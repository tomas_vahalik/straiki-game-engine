using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace StraikiEngine.Components
{
    public class Sprite: Component
    {
        private readonly String _texName;
        private Vector2 _position;

        public Vector2 Position
        {
            get { return _position; }
            set
            {
                _position = value;
                DestinationRectangle = new Rectangle((int)Position.X, (int)Position.Y, DestinationRectangle.Width, DestinationRectangle.Height);
            }
        }
            
        public Rectangle DestinationRectangle { get; set; }

        protected Texture2D Texture;

        public Sprite(String texName, Vector2 pos) : base("Sprite component", true)
        {
            _texName = texName;
            Position = pos;
        }
        public Sprite(String texName, Vector2 pos, int width, int height) : this(texName, pos)
        {
            DestinationRectangle = new Rectangle((int) Position.X, (int) Position.Y, width,height);
        }

        public override void Load()
        {
            Texture = Parent.Engine.ContentManager.Load<Texture2D>(_texName);
            if (DestinationRectangle.Width == 0 || DestinationRectangle.Height == 0) DestinationRectangle = new Rectangle((int)Position.X, (int)Position.Y, Texture.Width, Texture.Height);
            //base.Load();
        }

        public override void Draw()
        {
            Parent.Engine.SpriteBatch.Begin();
            Parent.Engine.SpriteBatch.Draw(Texture,DestinationRectangle,Color.White);
            Parent.Engine.SpriteBatch.End();

            Parent.Engine.GraphicsDevice.SamplerStates[0] = SamplerState.LinearWrap;
            Parent.Engine.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            Parent.Engine.GraphicsDevice.BlendState = BlendState.Opaque;
        }
    }
}
