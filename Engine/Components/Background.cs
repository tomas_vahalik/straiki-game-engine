using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace StraikiEngine.Components
{
    public class Background: Component
    {
        private readonly Color _color;

        public Background(Color color) : base("BG component", true)
        {
            _color = color; 
        }

        public override void Draw()
        {
            Parent.Engine.GraphicsDevice.Clear(_color);
        }
    }

}
