using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace StraikiEngine
{
    public class Component
    {
        private GameScreen _parent;
        private bool _loaded;


        public Component(string name) : this(name, true)
        {
        }

        public Component(string name, bool visible)
        {
            Name = name;
            Visible = visible;
        }

        public void LoadComponent()
        {
            if (_loaded) return;
            _loaded = true;

            Load();
        }

        public virtual void Load()
        {

        }

        public virtual void ChangeLevel()
        {
            
        }

        public string Name { get; set; }

        public bool Visible { get; set; }

        public GameScreen Parent
        {
            get { return _parent; }
            set
            {
                if (_parent == value) return;
                
                if(_parent != null) _parent.RemoveComponent(this);
                    _parent = value;
                if(value != null) _parent.AddComponent(this);
            }
        }

        public virtual void Update()
        {

        }
        public virtual void Draw()
        {
            if(Parent.Camera != null)
                Draw(Parent.Camera.View, Parent.Camera.Projection, Parent.Camera.Position);
        }
        public virtual void Draw(Matrix view, Matrix projection, Vector3 camPosition)
        {
            
        }
    }
}
