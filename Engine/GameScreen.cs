using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StraikiEngine.Cameras;

namespace StraikiEngine
{
    public class GameScreen
    {
        private string _name;
        private readonly List<Component> _components;
        private readonly List<Component> _updated;
        public bool Finished { get; set; }

        public virtual void NextLevel(){}

        public ArcBallCamera Camera { get; set; }

        public GameScreen(string name)
        {
            Name = name;
            _updated = new List<Component>();
            _components = new List<Component>();
        }

        public void AddComponent(Component c)
        {
            if (c == null || Components.Contains(c)) return;
            Components.Add(c);
            c.Parent = this;
            c.LoadComponent();
        }
        public void RemoveComponent(Component c)
        {
            if (c == null || !Components.Contains(c)) return;
            _components.Remove(c);
            c.Parent = null;
        }

        /// <summary>
        /// Load GS
        /// </summary>
        public void LoadGameScreen()
        {
            if(Loaded) return;
            Loaded = true;
            // rest stuff same for all game screens shoud be here
            Load();
        }

        /// <summary>
        /// Adding components for each inherited screen
        /// </summary>
        protected virtual void Load()
        {

        }


        /// <summary>
        /// Copy all components into tmp list, than update
        /// </summary>
        public virtual void Update()
        {
            _updated.Clear();

            foreach(var c in _components)
                _updated.Add(c);
            foreach (var c in _updated)
                c.Update();
            if (Camera != null) Camera.Update();
            
        }
        public virtual void Draw()
        {
            foreach (var c in _components.Where(c => c.Visible))
                c.Draw();
        }

        /**
         * Set & get
         */
        public string Name
        {
            get { return _name; }
            set { _name = !string.IsNullOrEmpty(value) ? value : "title"; }
        }

        public List<Component> Components
        {
            get { return _components; }
        }

        public bool Loaded { get; private set; }

        public Engine Engine { get; set; }
    }
}
