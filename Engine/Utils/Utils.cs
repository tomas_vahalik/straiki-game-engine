using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace StraikiEngine.Utils
{
    class Utils
    {
        /// <summary>
        ///  World = Scale*Rotation*Translation
        /// </summary>
        /// <param name="translation"></param>
        /// <returns></returns>
        public static Matrix CreateWorld(Vector3 translation)
        {
            return Matrix.CreateTranslation(translation);
        }

        public static Matrix CreateWorld(Vector3 translation, Matrix rotation)
        {
            return rotation * Matrix.CreateTranslation(translation);
        }

        public static Matrix CreateWorld(Vector3 translation, Matrix rotation, Vector3 scale)
        {
            return Matrix.CreateScale(scale) * rotation * Matrix.CreateTranslation(translation);
        }

        public static Matrix CreateRotation(Vector3 rotation)
        {
            return Matrix.CreateFromYawPitchRoll(rotation.Y, rotation.X, rotation.Z);
        }
    }

}
