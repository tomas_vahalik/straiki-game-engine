using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Devices;

namespace StraikiEngine
{
    public class Input
    {
        private TouchPanelCapabilities _tc;

        public Vector2 MoveDelta { get; set; }
        public Vector2 FlickDelta { get; set; }
        public float ScaleChange { get; set; }
        public bool DoubleTap { get; set; }
        public Point TouchPoint { get; set; }
        public bool Flick { get; set; }

        public Input()
        {
            TouchPanel.EnabledGestures =
                //GestureType.Hold |
                GestureType.Tap |
                GestureType.DoubleTap |
                GestureType.FreeDrag |
                GestureType.Flick |
                GestureType.Pinch;

            _tc = TouchPanel.GetCapabilities();

            DoubleTap = false;
            Flick = false;

        }

        public int GetMaxTouchCount()
        {
            return _tc.IsConnected ? _tc.MaximumTouchCount : 0;
        }

        public void Update()
        {
            TouchCollection touches = TouchPanel.GetState();

            TouchPoint = new Point(0,0);
            //new primary point down.
            //if (touches.Count > 0 && touches[0].State == TouchLocationState.Pressed)
            //{
            //    //this is new primary point
            //    touchPoint = new Point((int)touches[0].Position.X, (int)touches[0].Position.Y);
            //}
            while (TouchPanel.IsGestureAvailable)
            {
                //new gesture <GestureSample>
                var gesture = TouchPanel.ReadGesture();

                switch (gesture.GestureType)
                {
                    case GestureType.Tap:
                        TouchPoint = new Point((int)gesture.Position.X, (int)gesture.Position.Y);
                        break;
                    case GestureType.DoubleTap:
                        DoubleTap = !DoubleTap;

                        VibrateController.Default.Start(TimeSpan.FromMilliseconds(100));
                        break;
                    case GestureType.FreeDrag:
                        MoveDelta = gesture.Delta * new Vector2(0.01f);
                        break;
                    case GestureType.Flick:
                        Flick = true;
                        FlickDelta = gesture.Delta;
                        break;
                    case GestureType.Pinch:
                        // get the current and previous locations of the two fingers
                        var a = gesture.Position;
                        var aOld = gesture.Position - gesture.Delta;
                        var b = gesture.Position2;
                        var bOld = gesture.Position2 - gesture.Delta2;

                        // figure out the distance between the current and previous locations
                        var d = Vector2.Distance(a, b);
                        var dOld = Vector2.Distance(aOld, bOld);

                        // calculate the difference between the two and use that to alter the scale
                        ScaleChange = (d - dOld) * -1f;
                        break;

                }

            }
            if (touches.Count == 0)
            {
                ScaleChange = 0;
                MoveDelta = new Vector2(0f);
                FlickDelta = new Vector2(0f);
                Flick = false;
            }
        }
    }
}
