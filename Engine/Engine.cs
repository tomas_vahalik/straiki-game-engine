using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Xna.Framework.Media;

namespace StraikiEngine
{
    public class Engine
    {
        private GameTime _gameTime;
        private readonly Game _parent;
        private readonly GraphicsDevice _graphicsDevice;
        private readonly SpriteBatch _spriteBatch;
        private readonly ContentManager _contentManager;
        private readonly Input _input;
        private readonly SoundManager _soundManager;

        public List<GameScreen> Screens;

        public Engine(Game parent)
        {
            this._parent = parent;
            this._graphicsDevice = parent.GraphicsDevice;
            this._contentManager = parent.Content;
            this._spriteBatch = new SpriteBatch(_graphicsDevice);
            this.Screens = new List<GameScreen>();
            this._input = new Input();
            this._soundManager = new SoundManager(_contentManager);
        }



        public void Update(GameTime time)
        {
            this._gameTime = time;
            this._input.Update();

            foreach (GameScreen screen in Screens)
            {
                screen.Update();
            }
            this._soundManager.Update();
        }
        public void Draw(GameTime time)
        {
            this._gameTime = time;
            GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise;

            foreach (var screen in Screens)// from c in Screens where !c.Finished select c)
            {
                screen.Draw();
            }
        }

        public void PushGameScreen(GameScreen window)
        {
            if (window.Engine != null || Screens.Contains(window)) return;
            this.Screens.Add(window);
            window.Engine = this;
            window.LoadGameScreen();
        }

        public GameScreen PopGameScreen()
        {
            if (this.Screens.Count == 0) return null;

            //var GameScreen
            var retVal = Screens[Screens.Count - 1];
            retVal.Engine = null;
            Screens.Remove(retVal);
            return retVal;
        }


        /*
         * Setters and getters
         */
        public GameTime GameTime
        {
            get { return _gameTime; }
        }

        public Game Parent
        {
            get { return _parent; }
        }

        public GraphicsDevice GraphicsDevice
        {
            get { return _graphicsDevice; }
        }

        public SpriteBatch SpriteBatch
        {
            get { return _spriteBatch; }
        }

        public ContentManager ContentManager
        {
            get { return _contentManager; }
        }

        public Input Input
        {
            get { return _input; }
        }

        public SoundManager SoundManager
        {
            get { return _soundManager; }
        }
    }
}
